# Spring相关知识点

**1.Bean的生命周期？（重要）**

```java
AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);  //创建Spring容器

UserService userService = new applicationContext.getBean("userService",userService.class);
userService.test();
```

**描述一下Spring Bean 的生命周期**

1. Spring扫描指定包下面的java类，将其变成BeanDefinition对象
2. 如果有多个构造方法，则要推断构造方法
3. 确定好构造方法后，进行实例化一个对象
4. 对对象中加了@Autowired的注解的属性进行属性填充
5. 回调Aware方法，比如BeanNameAware，BeanFactoryAware
6. 调用BeanPostProcessor的初始化前的方法**执行postProcessBeforeInitialization()方法**
7. 调用初始化方法 如果实现了 **InitializingBean接口**执行**afterPropertiesSet方法**
8. 如果Bean在配置文件中的定义包含了init-method属性，执行指定的方法。
9. 调用BeanPostProcessor的初始化后的方法，在这里会进行AOP**执行postProcessAfterInitialization() 方法**
10. 如果当前创建的Bean是单例则会把Bean放入单例池中
11. 使用Bean
12. Spring容器关闭时调用DisposableBean中的destory方法
13. 当要销毁Bean的时候，如果Bean在配置文件中的定义包含destroy-method属性，执行指定的方法。

#### Spring配置文件

![image-20220405144126804](C:.\images\image-20220405144126804.png)

![image-20220405145840303](.\images\image-20220405145840303.png)

![image-20220405145906878](.\images\image-20220405145906878.png)

**依赖注入：**

- 构造方法注入
- set注入

#### SpringMVC的执行流程

1. 用户发送请求至前端控制器**DispatcherServlet**
2. **DispatcherServlet**收到请求调用**HandlerMapping**处理器映射器
3. 处理器映射找到具体的处理器（可以根据xml配置、注解进行查找），生成处理器对象及处理器拦截器（如果有则生成）一并返回给**DispatcherServlet**
4. **DispatcherServlet**调用**HandlerAdapter**处理器适配器
5. **HandlerAdapter**经过适配调用具体的处理器（**Controller**，也叫后端控制器）。
6. **Controller**执行完成后返回**ModelAndView**
7. **HandlerAdapter**将**Controller**执行完成后返回的**ModelAndView**返回给**DispatcherServlet**
8. **DispatcherServlet**将**ModelAndView**传给**ViewReslover**视图解析器
9. **ViewReslover**解析后返回具体的**View**
10. **DispatcherServlet**根据**View**进行渲染视图（将数据模型填充至视图中）**DispatcherServlet**响应用户。

![image-20220405223512364](C:.\images\image-20220405223512364.png)

#### 拦截器（Interceptor）与过滤器（Filter）的区别

![image-20220403150213523](.\images\image-20220403150213523.png)

1. 拦截器是基于java的反射机制的，而过滤器是基于函数的回调。
2. 拦截器不依赖于servlet容器，而过滤器依赖于servlet容器。
3. 拦截器只对action请求起作用，而过滤器则可以对几乎所有的请求起作用。
4. 拦截器可以访问action上下文、值、栈里面的对象，而过滤器不可以。
5. 在action的生命周期中，拦截器可以多次被调用，而过滤器只能在容器初始化时被调用一次。
6. 拦截器可以获取IOC容器中的各个bean，而过滤器不行，这点很重要，在拦截器里注入一个service，可以调用业务逻辑。
7. **触发时机：**过滤器是在请求进入容器后，但请求进入servlet之前进行预处理的。请求结束返回也是，是在servlet处理完后，返回给前端之前。过滤器包裹servlet，servlet包裹住拦截器。

**图解：**

![image-20220403150859713](.\images\image-20220403150859713.png)

![image-20220403150914959](.\images\image-20220403150914959.png)

![image-20220403150928453](.\images\image-20220403150928453.png)







写一个MyInterceptor1继承HandlerInterceptor 重写preHandle、postHandle、afterCompletion

![image-20220403231155847](.\images\image-20220403231155847.png)

拦截器的配置：配置到spring-mvc.xml上

![image-20220403230638110](.\images\image-20220403230638110.png)

#### AOP的理解

AOP面向切面编程，将代码中重复的部分抽取出来，使用动态代理技术，在不修改源码的基础上对方法进行增强。如果目标对象实现了接口，默认采用JDK动态代理，也可以强制使用CGLib,如果目标对象没有实现接口，采用CGLib的方式。常用的场景包括权限认证、自动缓存、错误处理、日志、调试和事务等
