# JVM

#### 1.内存溢出问题该如何解决？

 **内存溢出**是指应用系统中存在无法回收的内存或使用的内存过多，最终使得程序运行要用到的内存大于虚拟机能提供的最大内存。

  引起内存溢出的**原因**有很多种，常见的有以下几种：
1.内存中加载的数据量过于庞大，如一次从数据库取出过多数据；
2.集合类中有对对象的引用，使用完后未清空，使得JVM不能回收；
3.代码中存在死循环或循环产生过多重复的对象实体；
4.使用的第三方软件中的BUG；
5.启动参数内存值设定的过小；

内存溢出的**解决方案**：
**第一步**，修改JVM启动参数，直接增加内存。(-Xms，-Xmx参数一定不要忘记加。)

**第二步**，检查错误日志，查看“OutOfMemory”错误前是否有其它异常或错误。

**第三步**，对代码进行走查和分析，找出可能发生内存溢出的位置。

重点排查以下几点：
1.检查对数据库查询中，是否有一次获得全部数据的查询。一般来说，如果一次取十万条记录到内存，就可能引起内存溢出。这个问题比较隐蔽，在上线前，数据库中数据较少，不容易出问题，上线后，数据库中数据多了，一次查询就有可能引起内存溢出。因此对于数据库查询尽量采用分页的方式查询。
2.检查代码中是否有死循环或递归调用。

3.检查是否有大循环重复产生新对象实体。

4.检查对数据库查询中，是否有一次获得全部数据的查询。一般来说，如果一次取十万条记录到内存，就可能引起内存溢出。这个问题比较隐蔽，在上线前，数据库中数据较少，不容易出问题，上线后，数据库中数据多了，一次查询就有可能引起内存溢出。因此对于数据库查询尽量采用分页的方式查询。

5.检查List、MAP等集合对象是否有使用完后，未清除的问题。List、MAP等集合对象会始终存有对对象的引用，使得这些对象不能被GC回收。

#### 类加载的过程

加载--->验证--->准备--->解析--->初始化

**①、加载：**

1. 通过一个类的全限定名获取定义此类的二进制字节流
2. 将这个字节流所代表的的静态存储结构转化为方法区的运行时数据结构
3. **在内存中生成一个代表这个类的java.lang.Class对象**，作为方法区这个类的各种数据的访问入口。

**②、链接：包括验证、准备、解析：**

验证：

- 目的在于确保Class文件的二进制字节流中包含信息符合当前虚拟机要求，保证被类加载的正确性，不会危害虚拟机自身安全。
- 主要包括四种验证，文件格式验证，元数据验证、字节码验证，符号引用验证。

准备：

1. 为类变量分配内存并且设置该类变量的默认初始化值，即0或者null等根据数据类型。
2. 这里不包括final修饰的static，因为final在编译的时候就会分配了准备阶段会显式初始化
3. 这里也不会为实例变量分配初始化，（因为对象还没有创建），类变量会分配在方法区中，而实例变量是会随着对象一起分配到Java堆中。

解析：

- 将常量池内的符号引用转换为直接引用
- 事实上，解析操作往往会随着JVM在执行完初始化之后再进行
- 解析动作主要针对类、接口、字段、类方法、接口方法、方法类型等等。对应常量池中CONSTANT_Class_info、CONSTANT_Fieldref_info、CONSTANT_Methodref_info

**③、初始化：**

1. 初始化阶段就是执行类构造器方法<clinit>()的过程
1. 这个方法不需要定义，是javac编译器自动手记类中的所有类变量的赋值动作和静态代码块中的语句合并而来。
1. 构造器方法中指令按语句在源文件中出现的顺序执行
1. <clinit>()不同于类的构造器。（构造器是虚拟机视角下的<init>()）
1. 若该类具有父类，JVM会保证子类的<clinit>()执行前，父类的<clinit>()已经执行完毕。
1. 虚拟机必须保证一个类的<clinit>()方法在多线程下被同步加锁

#### 类加载器

1. **BootstrapClassLoader**（启动类加载器）
   1. 这个类加载器是使用c/c++实现的，嵌套在jvm内部
   2. 这个加载器用加载Java的核心库（JAVA_HOME/jre/lib/rt.jar)、resources.jar、sun.boot.class.path路径下的内容。用于提供JVM自身需要的类。
   3. 并不继承java.lang.ClassLoader，没有父加载器
   4. 加载扩展类和应用程序类加载器，并指定为他们的父类加载器。
   5. 出于安全考虑，只加载包名为java、javax、sun等开头的类
2. **ExtensionClassLoader**（扩展类加载器）
   1. Java语言编写，由sun.misc.Launcher$ExtClassLoader实现
   2. 派生于ClassLoader类
   3. 父类加载器为启动类加载器
   4. 从java.ext.dirs系统属性所指定的目录中加载类库，或从安装目录下/jre/lib/ext子目录下加载类库。如果用户创建的jar放在此目录下，也会自动由扩展类加载器加载。
3. **AppClassLoader**（应用程序类加载器）
   1. Java语言编写，由sun.misc.Launcher$ExtClassLoader实现
   2. 派生于ClassLoader类
   3. 父类加载器为扩展类加载器
   4. 他负责加载环境变量classpath或系统属性 java.class.path 指定路径下的类库
   5. 该类加载是程序中默认的类加载器一般来说，java应用的类都是由他完成
   6. 通过ClassLoader.getSystemClassLoader（）方法可以获取到该类加载器

**自定义类加载器：**

为什么需要:

- 隔离加载类
- 修改类加载的方式
- 扩展加载源
- 防止源码泄露

#### 双亲委派机制

**工作原理：**

1. 如果一个类加载器收到了类加载的请求，他并不会自己先去加载，而是把这个请求委托给父类加载器去执行。
2. 如果父类加载器还存在其父类加载器，则进一步向上委托，依次递归，请求最终将到达顶层的启动类加载器。
3. 如果父类加载器可以完成类加载器任务，就成功返回，如果不能，子加载器才会尝试自己去加载。

**沙箱安全机制：**

自定义String类，但是在加载自定义String类的时候会率先使用引导类加载器加载，而引导类加载器在加载的过程中会先加载jdk自带的文件，报错信息说没有main方法，是因为加载的是rt.jar包中的String类。这样就可以对java核心源代码进行保护，这就是沙箱安全机制。

**三次双亲委派机制的破坏：**



#### Minor GC（Young GC）、Major GC、Full GC

针对HotSpot Vm，将GC按照回收区域又分为两大类型：一种是部分回收（Partial GC），一种是整堆收集（Full GC）

**部分回收（Partial GC）：**

1. Minor GC（Young GC）：只是新生代（Edan\s0,s1）的垃圾收集
   - **触发机制**
     - 当年轻代空间不足时，会触发Minor GC，这里指的是Eden代满，Survivor满不会触发GC
2. Major GC（Old GC）：只是老年代的垃圾收集
   1. 目前，只有CMS GC会有单独收集老年代的行为
   2. 很多时候Major GC和Full GC混淆使用，需要具体分辨是老年代回收还是整堆回收
      - **触发机制**：当老年代空间不足时，会先尝试触发Minor MC。如果空间还是不够则触发Major GC
      - Major GC速度比Minor GC慢10倍以上
      - 如果Major GC后空间还不足。就会报OOM
3. 混合收集（Mixed GC）：收集整个新生代以及部分老年代的垃圾收集、
   1. 目前只有G1 GC会有这种行为

**整堆收集（Full GC）**：

整个java堆和方法区的垃圾收集

**触发机制：**

1. 调用System.gc()时，系统建议执行Full GC，但是不必然执行。
2. 老年代空间不足
3. 方法区空间不足
4. 通过Minor GC后进入老年代的平均大小大于老年代的可用内存
5. 由Eden区、survivor space0（From Space）区向survivor space1（To Space）区复制时，对象大小大于To Space可用内存，则把该对象转存到老年代中，且老年代的可用内存小于该对象大小。
