# SQL题目

**用户-视频互动表:	tb_user_video_log**

| id   | uid  | video_id | start_time          | end_time            | if_follow | if_like | if_retweet | comment_id |
| ---- | ---- | -------- | ------------------- | ------------------- | --------- | ------- | ---------- | ---------- |
| 1    | 101  | 2001     | 2021-10-01 10:00:00 | 2021-10-01 10:00:30 | 0         | 1       | 1          | NULL       |
| 2    | 102  | 2001     | 2021-10-01 10:00:00 | 2021-10-01 10:00:21 | 0         | 0       | 1          | NULL       |
| 3    | 103  | 2001     | 2021-10-01 11:00:50 | 2021-10-01 11:01:20 | 0         | 1       | 0          | 1732526    |
| 4    | 102  | 2002     | 2021-10-01 11:00:00 | 2021-10-01 11:00:30 | 1         | 0       | 1          | NULL       |
| 5    | 103  | 2002     | 2021-10-01 10:59:05 | 2021-10-01 11:00:05 | 1         | 0       | 1          | NULL       |

**短视频信息表:	tb_video_info**

| id   | video_id | author | tag  | duration | release_time        |
| ---- | -------- | ------ | ---- | -------- | ------------------- |
| 1    | 2001     | 901    | 影视 | 30       | 2021-01-01 07:00:00 |
| 2    | 2002     | 901    | 美食 | 60       | 2021-01-01 07:00:00 |
| 3    | 2003     | 902    | 旅游 | 90       | 2021-01-01 07:00:00 |

解释：

影视类视频2001被用户101、102、103看过，播放进度分别为：30秒（100%）、21秒（70%）、30秒（100%），平均播放进度为90.00%（保留两位小数）；

美食类视频2002被用户102、103看过，播放进度分别为：30秒（50%）、60秒（100%），平均播放进度为75.00%（保留两位小数）；

**注**：

- 播放进度=播放时长÷视频时长*100%，当播放时长大于视频时长时，播放进度均记为100%。
- 结果保留两位小数，并按播放进度倒序排序

**问题**：计算各类视频的平均播放进度，将进度大于60%的类别输出。

```sql
SELECT
	tag,
	CONCAT(
		round(
			avg(
			IF
			( TIMESTAMPDIFF( SECOND, start_time, end_time ) > duration, 1, TIMESTAMPDIFF( SECOND, start_time, end_time ) / duration ))* 100,
			2 
		),
		"%" 
	) AS avg_play_progress 
FROM
	tb_user_video_log a
	LEFT JOIN tb_video_info b ON a.video_id = b.video_id 
GROUP BY
	tag 
HAVING
	avg_play_progress > 60 
ORDER BY
	avg_play_progress DESC
```

**问题**：计算2021年里有播放记录的每个视频的完播率(结果保留三位小数)，并按完播率降序排序

```sql
SELECT
	tb_user_video_log.video_id,
	round( sum( IF ( end_time - start_time >= duration, 1, 0 ))/ count( start_time ), 3 ) AS avg_comp_play_rate 
FROM
	tb_user_video_log
	LEFT JOIN tb_video_info ON tb_user_video_log.video_id = tb_video_info.video_id 
WHERE
	YEAR ( start_time ) = 2021 
GROUP BY
	tb_user_video_log.video_id 
ORDER BY
	avg_comp_play_rate DESC
```

