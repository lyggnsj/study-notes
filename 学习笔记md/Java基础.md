# Java基础

#### 类的五大成分（有且只有）：

1. 成员变量
2. 成员方法
3. 内部类
4. 代码块
5. 构造器

#### This关键字

**this的作用**

- this代表了当前对象的引用
- 可以用在实例方法和构造器中
- 用在方法中，谁调用这个方法，this就代表谁
- this用在构造器中，代表了构造器正在初始化的那个对象的引用

**private修饰的方法，成员变量、构造器只能在本类直接访问。**

#### static关键字

Java是通过成员变量是否有static来区别属于类还是每个对象的

static === 静态 === 修饰的成员（方法和成员变量）属于类本身

![image-20220405233124940](.\images\image-20220405233124940.png)

**成员变量访问内存图：**

![image-20220406002918413](.\images\image-20220406002918413.png)

**成员方法的访问内存图：**

![image-20220406144612157](.\images\image-20220406144612157.png)

#### 继承

有争议的观点

1. 子类是否可以继承父类的私有成员变量、私有成员方法

   - ![image-20220406150508788](.\images\image-20220406150508788.png)

   子类可以继承父类的私有方法、只是没有办法直接访问。（儿子继承了父亲的保险柜但是没有密码打不开）可以通过反射暴力访问

2. 子类是否可以继承父类的静态成员（变量、方法）

   - ![image-20220406150943815](.\images\image-20220406150943815.png)
   - 子类是不能继承父类的静态成员：存在父类的静态域、只有一份。可以共享所以可以访问。

**JDK8新特性**

```java
遍历
list.forEach(str-> System.out.println(str));
排序
Collections.sort(list,(o1,o2)->{return  o1.compareTo(o2);});
```

