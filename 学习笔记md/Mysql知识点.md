# Mysql进阶知识点

### 1.存储引擎

1. **InnoDB**
   1. DML（数据的增删改）操作遵循ACID模型，支持事务。
   2. 行级锁，提高并发访问的性能
   3. 支持外键FOREIGN KEY约束，保证数据的完整性和正确性。
2. **MyISAM**
   1. 不支持事务，不支持外键
   2. 支持表锁，不支持行锁
   3. 访问速度快
3. **Memory**
   1. 内存存放 访问速度很快
   2. 支持hash索引（默认）

### 2.如何选择存储引擎

- InnoDB：对事务的完整性有比较高的要求，在并发条件下要求数据的一致性。
- MyISAM：以读操作和插入操作为主，对事务的完整性和并发条件下的数据的一致性要求不高
- Memory：访问速度很快，一般用于临时表或者缓存 对表的大小有严格的限制

### 3.索引

**优点：**

1. 提高数据检索的效率，降低数据库的IO成本。
2. 通过索引列对数据进行排序，降低数据排序的成本，降低CPU的消耗。

**缺点：**

1. 索引列占用空间
2. 索引提高了查询效率，同时降低了更新表的效率

**索引结构：**

1. B+Tree索引
2. Hash索引
3. R-Tree（空间索引）
4. Full-text（全文索引）

**二叉树：**

**缺点：**顺序插入时,会形成一个链表,查询性能大大降低.大数据量的时候,层间很深,检索速度慢.

**红黑树:本质上也是一种二叉树**

**缺点:**大数据量的时候,层间很深,检索速度慢.

**B-Tree(多路平衡查找树):**

**树的度数指的是一个节点的子节点个数.最大度数为5的B树(每个节点)最多存储4个key,5个指针。当超过自多存储的key值时，向上裂变。**[动画演示地址](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)

**B+Tree**

所有的元素都会出现在叶子结点，叶子结点形成一个单向链表

**Hash索引**：

1. Hash索引只能用于对等比较（=，in）不支持范围查询（Between，>，<，等）
2. 无法利用索引完成排序的操作
3. 查询效率高，通常只需要一次检索就可以了，效率通常要高于B+Tree索引

#### 为什么InnoDB存储引擎选择使用B+Tree索引结构？

- 相对于二叉树，层级更少，搜索效率高
- 相对于B-Tree，无论是叶子结点还是非叶子结点，都会保存数据，这样导致一页中存储的键值减少，指针减少，同意保存大量数据的时候，只会增加树的高度，导致性能降低。
- 相对于Hash索引，他只支持等值匹配，不支持范围查询。

#### 索引分类：

- 主键索引（只能有一个）PRIMARY
- 唯一索引（避免同一个表中某数据列中的值重复，可以有多个）UNIQUE
- 常规索引（快速定位数据，多个）
- 全文索引（查找的文本中的关键字，而不是比较索引中的值，多个）FULLTEXT

在InnoDB引擎中，根据索引存储形式，又可以分为两种：

- 聚集索引（将数据存储与索引放到一块，索引结构的**叶子结点保存了行数据**）必须要有且只有一个
- 二级索引（非聚集索引）（将数据与索引分开存储，索引结构的**叶子结点关联的是对应的主键**）可以有多个

**聚集索引选取规则：**

- 如果存在主键，主键索引就是聚集索引
- 如果不存在主键，将使用第一个唯一索引作为聚集索引
- 如果没有主键，也没有合适的唯一索引，InnoDB会自动生成一个rowid作为隐藏的聚集索引

回表查询：先走二级索引查出主键，再根据主键查出需要的行数据

InnoDB中一页16kb一个指针占用6个字节

#### SQL性能分析

**1.查看慢查询日志、show profile 查看耗时**

**2.查看SQL语句的执行计划：explain**

**例如：explain select * from student s where s.id in(select studentid from student_course sc where sc.courseid = (select id from course c where c.name = 'MySQL'));**

![image-20220329224609757](C:.\images\image-20220329224609757.png)

**id越大执行优先级越高,id值相同的话从上到下执行**

**select_type:**

- SIMPLE:简单表，不需要表连接或者子查询
- PRIMARY：主查询，即外层的查询
- UNION：UNION中的第二个或者后面的查询语句
- SUBQUERY(SELECT/WHERE之后包含了子查询)等

**type：**

表示连接类型，性能由好到差为NULL、system、const、eq_ref、ref、range、index、all

**possible_key：**

显示可能应用在这张表上的索引，一个或者多个。

**key：**

实际孤岛的索引，如果为null，则没有使用索引

**key_len：**

使用到的索引的字节数，该值为索引字段最大可能的长度，并非实际使用长度，在不损失精确性的前提下，长度越短越好

#### 索引使用

- **最左前缀法则**（如果索引了多列（联合索引），要遵循最左前缀法则。最左前缀法则指的是查询从索引的最左列开始，并且不跳过索引中的列。如果跳过某一列，索引将部分失效（后面的字段索引失效））。
- **范围查询：**联合索引中，出现范围查询（>,<）范围查询右侧的索引失效。**尽量使用>=或者<=**
- **索引列运算：**不要再索引上进行运算操作，索引将失效。
- **字符串不加引号：**字符串类型字段使用时，不加引号，索引将失效
- **模糊查询：**如果仅仅是尾部模糊匹配，索引不会失效，如果头部模糊匹配，索引失效。
- **or连接的条件：**用or分隔开的条件，如果or前的条件中的列有索引，而后面的列中没有索引，那么涉及的索引都不会被用到。
- **数据分布影响：**如果Mysql评估走索引比全表扫描慢将不使用。
- **SQL提示：**

#### SQL中的执行顺序

**FROM>ON>JOIN>WHERE>GROUP BY>WITH CUBE or WITH ROLLUP>HAVING>SELECT>DISTINCT>ORDER BY>TOP**

#### MsqlDDL相关语句

```mysql
增加列
ALTER TABLE gov_health ADD health_test VARCHAR(200) not null COMMENT "ceshi" ;
修改列的数据类型和列的名称  如果只修改类型可以用 modify
ALTER TABLE gov_health CHANGE COLUMN health_test test varCHAR(120);
删除列
ALTER TABLE gov_health DROP COLUMN test;
```

#### SQL优化

1. 插入优化
   1. 批量插入 不要一次插入一条
   2. 手动提交事务
   3. 主键顺序插入
   4. 大批量插入数据使用load
      1. ![image-20220404182617242](C:.\images\image-20220404182617242.png)
2. 
